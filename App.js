import React from 'react';
import { StyleSheet, Text, View, FlatList, Dimensions, TouchableOpacity, Image } from 'react-native';
import AfficheItem  from './Components/AfficheItem'
import Recherche  from './Components/Recherche'
import List2Column  from './Components/List2Column'
import Navigation from './Navigation/Navigation'
import Onboarding from 'react-native-onboarding-screen'
import { Provider } from 'react-redux'
import Store from './Store/configureStore'
const ITEM_WIDTH = Dimensions.get('window').width
class App extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      films: [],
      columns:2
    }
  }

//   render(){
//     const {films, columns} = this.state

//     //console.log(films);
//   return (
//     <Navigation/>

//   );
// }
finishOnboarding(){
  console.log("test");
}
render() {
        const Pages = [
                {
                  //image: <Image source={require('./Images/icon.png')} />,
                    title: 'Welcome',
                    subtitle: 'to the Oxford Union app. Here you can browse the term card and engage with the Union, all in one place.'
                },
                {
                    title: 'Get Engaged',
                    subtitle: 'The Oxford Union app integrates closely with Facebook. Don\'t worry, we won\'t post anything to your profile.',
                    action: {
                        title: "Login to Facebook",
                        onPress: this.showFacebookLogin
                    }
                },
                {
                    title: "Great",
                    subtitle: "That's all for now. As you use the app, we'll learn about the events you like and use this to personalize your experience.",
                    action: {
                        title: "Let's Get Started",
                        onPress: this.finishOnboarding
                    }
                }
        ];
        return (
           // <Onboarding
            //    backgroundImage={require('./Images/icon.png')}
            //    pages={Pages}
            ///>

            <Provider store={Store} >
            <Navigation/>
            </Provider>
        );
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default App
