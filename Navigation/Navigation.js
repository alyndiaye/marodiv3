// Navigation/Navigation.js
import React from 'react' // N'oubliez pas l'import de React ici. On en a besoin pour rendre nos components React Native Image !
import { StyleSheet, Image } from 'react-native';
//import Image from 'react-native-remote-svg'
import Icon from 'react-native-vector-icons/Ionicons'
import { createStackNavigator, createAppContainer, createBottomTabNavigator, Dimensions } from 'react-navigation'
import Accueil from '../Components/Accueil'
import Genres from '../Components/Genres'
import Recherche from '../Components/Recherche'
import Favories from '../Components/Favories'
import Profil from '../Components/Profil'
import Player from '../Components/Player'
import DetailVideo from '../Components/DetailVideo'
import ListeSerie from '../Components/ListeSerie'
import ListeVideo from '../Components/ListeVideo'
import DetailSerie from '../Components/DetailSerie'
import Login from '../Components/Login'
import Register from '../Components/Register'
import SearchContainer from '../Components/SearchContainer'
import ActionBarImage from './ActionBarImage'
import '../Utils/global.js'

const MainNavigator = createStackNavigator({
  Accueil: {screen: Accueil},
  Player: {screen: Player},
  DetailVideo: {screen: DetailVideo},
});

const GenreNavigator = createStackNavigator({
  Genre: {screen: Genres,
    navigationOptions: {
      title: 'Marodi',
      headerLeft: <ActionBarImage />,
      headerStyle: {
      backgroundColor: global.bgcNavBar,

    },
    headerTitleStyle: {
    fontWeight: '500',
      color: '#FFFFFF',
      fontSize: 20,
      lineHeight: 28
    },
    }
  },
  ListeSerie: {screen: ListeSerie,
    navigationOptions: {
      title: 'Serie',
    headerStyle: {
      backgroundColor: global.bgcNavBar,
    },
    headerTitleStyle: {
      fontWeight: '500',
      color: '#FFFFFF',
      fontSize: 20,
      lineHeight: 28
    },
   
    }

  },
  DetailSerie: {screen: DetailSerie},
  ListeVideo: {screen: ListeVideo},
});

const ProfilNavigator = createStackNavigator({
    Profil: {
      screen: Profil,
      navigationOptions:{
        title: 'Marodi',
      headerLeft: <ActionBarImage />,
      headerStyle: {
      backgroundColor: global.bgcNavBar,

    },
    headerTitleStyle: {
      fontWeight: '500',
      color: '#FFFFFF',
      fontSize: 20,
      lineHeight: 28
    },
      }
    },
    Login: {screen: Login},
    Register: {screen: Register},
    Favories: {screen: Favories,
    navigationOptions: {
      title: 'Favories',
      headerLeft: <ActionBarImage />,
      headerStyle: {
      backgroundColor: global.bgcNavBar,

    },
    headerTitleStyle: {
      fontWeight: '500',
      color: '#FFFFFF',
      fontSize: 20,
      lineHeight: 28
    },
    }},
  });

const MarodiTabNavigator = createBottomTabNavigator({
  Accueil: {
    screen: MainNavigator,
    navigationOptions: {
        tabBarIcon: ({tintColor}) => { // On définit le rendu de nos icônes par les images
          return <Image
            source={require('../Images/home.png')}
            style={styles.icon, {tintColor}}/>
        }
    }
  },
  Genres: {
    screen: GenreNavigator,
    navigationOptions: {
        tabBarIcon: ({tintColor}) => { // On définit le rendu de nos icônes par les images
          return <Image
            source={require('../Images/categorieb.png')}
            style={styles.icon, {tintColor}}/>
        }
    }
  },
  Recherche: {
    screen: Recherche,
    navigationOptions: {
        tabBarIcon: ({tintColor}) => { // On définit le rendu de nos icônes par les images
          return <Image
            source={require('../Images/search.png')}
            style={styles.icon, {tintColor}}/>
        }
    }
  },
  
  
  Profil: {
    screen: ProfilNavigator,
    navigationOptions: {
        tabBarIcon: ({tintColor}) => { // On définit le rendu de nos icônes par les images
          return <Image
            source={require('../Images/profil.png')}
            style={styles.icon, { tintColor }}/>
        }
    }
  }
  // Favories: {
  //   screen: Favories,
  //   navigationOptions: {
  //       tabBarIcon: ({tintColor}) => { // On définit le rendu de nos icônes par les images
  //         return <Image
  //           source={require('../Images/favorie.png')}
  //           style={styles.icon, {tintColor}}/>
  //       }
  //   }
  // },
},
{
  tabBarOptions: {
    activeTintColor: '#F1B331',
    inactiveTintColor: 'white',
    showLabel: true, // On masque les titres
    showIcon: true, // On informe le TabNavigator qu'on souhaite afficher les icônes définis
    style: {
      backgroundColor: bgcBottomBar,
      height: 56
  },
  //activeBackgroundColor: '#DDDDDD', // Couleur d'arrière-plan de l'onglet sélectionné
  //inactiveBackgroundColor: '#FFFFFF', // Couleur d'arrière-plan des onglets non sélectionnés

  }
})
const styles = StyleSheet.create({
  icon: {
    width: 30,
    height: 30
  }
})


export default createAppContainer(MarodiTabNavigator)
