//This is an example code to show Image Icon in TextInput// 
import React, { Component } from 'react';
import {connect} from 'react-redux'

//import react in our code.

import { StyleSheet, View, TextInput, Image, ImageBackground, Text, Dimensions, TouchableOpacity } from 'react-native';
import logo from '../assets/logo.png' 
import Icon from 'react-native-vector-icons/Ionicons'

const {width: WIDTH} = Dimensions.get('window')
class Login extends React.Component {
  static navigationOptions = {
    header: null
  }
  constructor(props) {
    super(props);
    
    this.state = {
      username: '',
      password: '',
      user: undefined

    };
  }
  _onLogin(){
    const {username, password} = this.state
    fetch('https://www.marodi.tv/api/auth/login', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        email: username,
        password: password,
      }),
    }).then((response) => response.json())
    .then((responseJson) => {
      this.setState({
          user: responseJson,
          
        }
        )
      if (this.state.user.id!==undefined) {
       const action = {type: "CONNECT_USER", value: this.state.user}
       this.props.dispatch(action)
       this.props.navigation.navigate('Profil')
      } else {
        console.log("ko")
      }
      
    })
    .catch((error) => {
      console.error(error);
    });
  }
  render() {
    //console.log(this.props)
    return (
      <ImageBackground style={styles.backgroundContainer}>
      <View style={styles.logoContainer}>
      <Image source={logo} style={styles.logo} />
      <Text style={styles.logoText}>BIENVENUE</Text>
      </View>
      <View style={{marginTop:82}}>
      <TextInput
      style={styles.input}
      placeholder={'Username'}
      keyboardType="email-address"
      onChangeText={(username) => this.setState({ username })}
      />
      </View>
      <View style={{marginTop:18}}>
      <TextInput
      style={styles.input}
      placeholder={'Password'}
      secureTextEntry={true}
      onChangeText={(password) => this.setState({ password })}
      />
      <Image style={styles.btnEye} source={require('../assets/eye.png')} size={26}/>
      
      </View>

      <TouchableOpacity  style={styles.btnLogin} onPress={this._onLogin.bind(this)}>
      <Text style={styles.text}>
      SE CONNECTER
      </Text>

      </TouchableOpacity>
      </ImageBackground>
      );
    }
  }
  
  const styles = StyleSheet.create({
    backgroundContainer:{
      flex: 1,
      alignItems: 'center',
      backgroundColor: '#191919',
    },
    logoContainer:{
      marginTop: 66,
      alignItems: 'center',
    },
    logo:{
      width: 120,
      height: 120,
    },
    logoText:{
      color: '#E7E7E7',
      fontSize: 18,
      letterSpacing: 0.7,
      lineHeight: 23,
      fontWeight: '500'
    },
    input:{
      marginLeft: 16,
      marginRight: 16,
      borderRadius: 2,
      backgroundColor: '#222222',
      height: 46,
      width: WIDTH-32,
      letterSpacing: 0.5,
      color: '#B4B4B4',
      lineHeight: 18,
      fontSize: 14,
      fontWeight: '300', 
      paddingLeft: 12
    },
    btnEye:{
      position: 'absolute',
      width: 35,
      height: 45,
      right:37
    },
    btnLogin:{
      backgroundColor: '#1DB954',
      borderRadius: 2,
      height:46,
      width: WIDTH-32,
      marginTop: 30,
      justifyContent: 'center',
      alignItems: 'center',
    },
    text:{
      color: '#191919',
      fontWeight: '500',
      fontSize:16,
      lineHeight:20,


    }
    
  });

  const mapStateToProps = state => {
  return {
    user: state.user
  }
}
export default connect(mapStateToProps)(Login)
