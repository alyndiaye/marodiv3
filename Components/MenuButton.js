
import React, { Component } from 'react';
import { Image } from 'react-native';
import { PropTypes } from 'prop-types';
import Icon from 'react-native-vector-icons/FontAwesome';
import {
  View,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';

export default class MenuButton extends Component {
  constructor(props) {
  	super(props);
  	this.state = { addedToFavorite: false };

    this.addToFavorite = this.addToFavorite.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    this.setState({ addedToFavorite: nextProps.selected });
  }

  addToFavorite() {
  //  this.setState({
    //  addedToFavorite: !this.state.addedToFavorite,
   // }, () => {
      this.props.onPress();
   // });
  }

  render() {
  	//const { addedToFavorite } = this.state;
  	//const { color, selectedColor, onPress } = this.props;

    return (
      <TouchableOpacity
        onPress={this.addToFavorite}
      >
        <View>
          <Image 
            source={require('../assets/menu_icon.png')}
            //color={addedToFavorite ? 'white' : 'blue'}
            size={18}
            style={{width:30, height:20}}
          />

          
        </View>
      </TouchableOpacity>
    );
  }
}



const styles = StyleSheet.create({
  selectedColor: {
    position: 'absolute',
    left: 0,
    top: 0,
  },
});