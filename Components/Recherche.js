
import React from 'react'
import { StyleSheet, View, Text, Dimensions, FlatList, TextInput, Button, Image, TouchableOpacity } from 'react-native'
import List2Column from './List2Column'
const ITEM_WIDTH = Dimensions.get('window').width

class Recherche extends React.Component {

  constructor(props){
    super(props);
    this.searchedText = '',
    this.state = {
      films: [],
      columns: 2
    }
  }

  componentDidMount(){
    fetch('https://www.marodi.tv/api/mostView')
      .then(response => response.json())
      .then(data => this.setState({
        films: data.videos,
        isLoading: false
       }));

  }

  _displayLoading(){

  }

  _searchFilms(){
      fetch('https://www.marodi.tv/api/results?search_query='+this.searchedText)
      .then(response => response.json())
      .then(data => this.setState({
        films: data.videos,
        isLoading: false
       }));
  }

  _displayFilm= (video) => {
    this.props.navigation.navigate('Player', {video: video})
  }

  _searchTextInputChanged(text) {
    this.searchedText = text
  }

  render() {
    const {films, columns} = this.state
    //console.log(this.props.navigation)
    return (
      <View style={styles.main_container}>
        <View style={styles.SectionStyle}>
          <TextInput
            style={{ flex: 1, padding:10 }}
            placeholder="Recherche"
            underlineColorAndroid="transparent"
            onChangeText={(text) => this._searchTextInputChanged(text)}
          />
          <TouchableOpacity onPress={() => this._searchFilms()}>
           <Image
            source={require('../assets/search.png')}
            style={styles.ImageStyle}
          />
        </TouchableOpacity>
        </View>
        <View style={{marginTop:29, marginLeft: 16, flexDirection: 'row'}}>
        <Image
            //We are showing the Image from online
            source={
              require('../assets/logo.png')
              
        }
            style={styles.ImageIconStyle}
          />
          <Text style={styles.TextStyle}> Les plus regardées </Text>
        </View>

        <View style={{flex: 1, marginTop:17}}>
          <FlatList
            numColumns={columns}
            data={films}
            keyExtractor={(item) => item.id.toString()}
            renderItem={({item}) => 
               <List2Column itemWidth={(ITEM_WIDTH-20)/columns} film={item} 
               displayFilm={this._displayFilm}
               navigation={this.props.navigation}

              />
            
          }
          />
          {this._displayLoading()}
        </View>

      </View>
    )
  }
}

const styles = StyleSheet.create({
  main_container: {
    flex: 1,
    backgroundColor: '#191919'
  },
  textinput:{

  },
   container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    margin: 10,
  },
 
  SectionStyle: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#4F4F4F',
    height: 41,
    borderRadius: 3,
    marginTop: 33,
    marginLeft:5,
    marginRight:5
  },
 
  ImageStyle: {
    padding: 10,
    margin: 5,
    height: 25,
    width: 25,
    resizeMode: 'stretch',
    alignItems: 'center',
  },
ImageIconStyle: {
    width: 14,
    height:18,
    resizeMode: 'stretch',
  },
  TextStyle: {
    flex:1,
   color: '#FDFDFD',
    fontSize:20, 
    
    fontWeight: 'bold'
  },

})

export default Recherche
