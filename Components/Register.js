//This is an example code to show Image Icon in TextInput// 
import React, { Component } from 'react';
//import react in our code.
 
import { StyleSheet, View, TextInput, Image, ImageBackground, Text, Dimensions, TouchableOpacity } from 'react-native';
import logo from '../assets/logo.png' 
import Icon from 'react-native-vector-icons/Ionicons'

 const {width: WIDTH} = Dimensions.get('window')
export default class Register extends React.Component {
  static navigationOptions = {
        header: null
    }
    constructor(props) {
      super(props);
    
      this.state = {};
    }
  render() {
   
    return (
      <ImageBackground style={styles.backgroundContainer}>
        <View style={styles.logoContainer}>
          <Image source={logo} style={styles.logo} />
          <Text style={styles.logoText}>BIENVENUE</Text>
        </View>
        <View style={{marginTop:38}}>
        <TextInput
        style={styles.input}
        placeholder={'Prenon & Nom'}
        />
        </View>
        <View style={{marginTop:18}}>
        <TextInput
        style={styles.input}
        placeholder={'Email'}
        />        
        </View>
        <View style={{marginTop:18}}>
        <TextInput
        style={styles.input}
        placeholder={'Mot de passe'}
        secureTextEntry={true}
        />        
        </View>
        <View style={{marginTop:18}}>
        <TextInput
        style={styles.input}
        placeholder={'Confirmation'}
        secureTextEntry={true}
        />        
        </View>

        <TouchableOpacity  style={styles.btnLogin}>
        <Text style={styles.text}>
          S'INSCRIRE
        </Text>

        </TouchableOpacity>
      </ImageBackground>
    );
  }
}
 
const styles = StyleSheet.create({
  backgroundContainer:{
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#191919',
  },
  logoContainer:{
    marginTop: 66,
    alignItems: 'center',
  },
  logo:{
    width: 120,
    height: 120,
  },
  logoText:{
    color: '#E7E7E7',
    fontSize: 18,
    letterSpacing: 0.7,
    lineHeight: 23,
    fontWeight: '500'
  },
  input:{
    marginLeft: 16,
    marginRight: 16,
    borderRadius: 2,
    backgroundColor: '#222222',
    height: 46,
    width: WIDTH-32,
    letterSpacing: 0.5,
    color: '#B4B4B4',
    lineHeight: 18,
    fontSize: 14,
    fontWeight: '300', 
    paddingLeft: 12
  },
  btnEye:{
    position: 'absolute',
    top:8,
    right:37
  },
  btnLogin:{
    backgroundColor: '#1DB954',
    borderRadius: 2,
    height:46,
    width: WIDTH-32,
    marginTop: 30,
      justifyContent: 'center',
    alignItems: 'center',
  },
  text:{
color: '#191919',
fontWeight: '500',
fontSize:16,
lineHeight:20,


  }
  
});