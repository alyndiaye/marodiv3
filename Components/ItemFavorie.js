
import React from 'react'
import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native'

class ItemFavorie extends React.Component {



  render() {
  	const {film, toggleFavorite, displayFilm} = this.props
  	console.log(toggleFavorite)
    return (
      <View style={styles.main_Container}>
      <TouchableOpacity onPress={()=>displayFilm(film)}>

      <Image source={{uri: film.url_vignette}} style={styles.imageStyle}/>
      </TouchableOpacity>

      <View style={styles.descContainer}>
      <View style={styles.metaContainer} >
      <TouchableOpacity onPress={()=>displayFilm(film)}>
      <Text style={styles.nameStyle} >{film.name}</Text>
      <Text style={styles.descStyle} >{film.cat === 'Series' ? film.serie.name : film.cat}</Text>
      </TouchableOpacity>
      </View>
      
      <TouchableOpacity onPress={()=>toggleFavorite("TOGGLE_FAVORITE", film)}>
      <Image source={require('../assets/cross-circular-button-outline.png')} style={styles.iconStyle} />
      </TouchableOpacity>
      </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({

	main_Container:{
		flex:1,
		flexDirection: 'row',
		marginTop: 18,
		marginLeft: 16,
		marginRight:16

	},
	imageStyle:{
		height:55,
		width:100,
		borderRadius: 2,
	},
	descContainer:{
		flexDirection: 'row',
		height: 55,
		flex: 1,
		alignItems: 'center'

	},
	metaContainer:{
		flex:1,
		height:55,
		marginLeft:14
	},
	iconStyle:{
		width:14,
		height:18,
	},
	nameStyle:{
		color: '#FDFDFD',
		fontSize: 13,
		fontWeight: '300',
		lineHeight: 16,
		marginTop: 13
	},
	descStyle:{
		color: '#7A7A7A',
		fontSize: 11,
		fontWeight: '300',
		lineHeight: 14,
		marginTop: 1
	}
})

export default ItemFavorie
