
import React from 'react'
import { StyleSheet, Text, View, FlatList } from 'react-native'
import ItemFavorie from './ItemFavorie'
import { connect } from 'react-redux'
import Image from 'react-native-remote-svg'


class Favories extends React.Component {
	constructor(props) {
	  super(props);
	
	  this.state = {};
	  this._toggleFavorite = this._toggleFavorite.bind(this)
	}

	_toggleFavorite(type, value){
		const action = {type: type, value: value}
        this.props.dispatch(action)

	}
  _displayFilm= (video) => {
    this.props.navigation.navigate('Player', {video: video})
  }

  render() {
  	//console.log(this.props.favoritesFilm)
    return (

      <View style={styles.mainContainer} >
{
      
  this.props.favoritesFilm.length === 0 ? <Image source={require('../assets/emptyState.svg')} />   : <FlatList
      data={this.props.favoritesFilm}
      keyExtractor={(item) => item.id.toString()}
      renderItem={({item}) => 
      <ItemFavorie film={item} toggleFavorite={this._toggleFavorite}

      displayFilm={this._displayFilm}
      navigation={this.props.navigation}
       />

    }
    />
}
      	
      

      </View>
    )
  }
}

const styles = StyleSheet.create({
	mainContainer:{
		flex:1,
		backgroundColor: '#191919',

	},
  emptyContainer:{
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1
  }
})
const mapStateToProps = state => {
  return {
    favoritesFilm: state.favoritesFilm
  }
}

export default connect(mapStateToProps)(Favories)
