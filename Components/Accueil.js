
import React from 'react'
import { StyleSheet, Text, View, FlatList, Dimensions, TouchableOpacity, ActivityIndicator, Share } from 'react-native';
import AfficheItem  from './AfficheItem'
import List2Column  from './List2Column'
import EmissionItem  from './EmissionItem'
import FilmItem  from './FilmItem'
const ITEM_WIDTH = Dimensions.get('window').width

class Accueil extends React.Component {
  static navigationOptions = {
    header: null
  }
  constructor(props){
    super(props);
    this.state = {
      films: [],
      columns:2,
      typeItem: "episode",
      isLoading: false,
      colorEp: '#FFFFFF',
      colorEm: '#7A7A7A',
      colorMo: '#7A7A7A',
    }
  }
  componentDidMount() {
    this.setState({ isLoading: true })
    fetch('https://www.marodi.tv/api/categorie/serie')
    .then(response => response.json())
    .then(data => this.setState({
      films: data.videos,
      isLoading: false,
      typeItem: "episode"
    }));
  }
  _loadEmission= () =>{
    this.setState({ isLoading: true })
    fetch('https://www.marodi.tv/api/emissions')
    .then(response => response.json())
    .then(data => this.setState({
      films: data.videos,
      isLoading: false,
      typeItem: "emission",
      colorEp: '#7A7A7A',
      colorEm: '#FFFFFF',
      colorMo: '#7A7A7A',
    }));
  }
  _loadEpisode= () => {
    this.setState({ isLoading: true })
    fetch('https://www.marodi.tv/api/categorie/serie')
    .then(response => response.json())
    .then(data => this.setState({
      films: data.videos,
      isLoading: false,
      typeItem: "episode",
      colorEp: '#FFFFFF',
      colorEm: '#7A7A7A',
      colorMo: '#7A7A7A',
    }));
  }
  _loadFilm= () => {
    this.setState({ isLoading: true })
    fetch('https://www.marodi.tv/api/categorie/makingoff')
    .then(response => response.json())
    .then(data => this.setState({
      films: data.videos,
      isLoading: false,
      typeItem: "film",
      colorEp: '#7A7A7A',
      colorEm: '#7A7A7A',
      colorMo: '#FFFFFF',
    }));
  }

  _displayFilm= (video) => {
    this.props.navigation.navigate('Player', {video: video})
  }
  _displayLoading() {
    if (this.state.isLoading) {
      return (
        <View style={styles.loading_container}>
        <ActivityIndicator size='large' color='#F1B331' />
        </View>
        )
      }
    }

    render() {
      const {films, columns, typeItem} = this.state
      //console.log(this.props.navigation);
      return (
      <View style={{flex:1, backgroundColor:'#191919' }}>
      <AfficheItem displayFilm={this._displayFilm}
      navigation={this.props.navigation} />
      <View style={{height:60, backgroundColor: '#191919',  flexDirection: 'row'}}>

      <View style={styles.viewContainer}>
      <TouchableOpacity onPress={this._loadEpisode}>
      <Text style={styles.default_text, {color: this.state.colorEp}}>
      Episodes
      </Text>
      </TouchableOpacity>
      </View>

      <View style={styles.viewContainer}>
      <TouchableOpacity onPress={this._loadEmission}>
      <Text style={styles.default_text, {color: this.state.colorEm}}>
      Emissions
      </Text>
      </TouchableOpacity>
      </View>

      <View style={styles.viewContainer}>
      <TouchableOpacity onPress={this._loadFilm}>
      <Text style={styles.default_text, {color: this.state.colorMo}}>
      Making of
      </Text>
      </TouchableOpacity>
      </View>

      </View>
      <View style={{flex: 1}}>
      <FlatList
      numColumns={columns}
      data={films}
      keyExtractor={(item) => item.id.toString()}
      renderItem={({item}) => 
      <List2Column itemWidth={(ITEM_WIDTH-20)/columns} film={item} typeItem={typeItem}
      displayFilm={this._displayFilm}
      navigation={this.props.navigation}
      />

    }
    />
    {this._displayLoading()}
    </View>
    </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  viewContainer:{
    marginLeft: 8,
    marginRight: 8,
    marginTop: 26,
  },
  default_text: {
    fontWeight: '500',
    fontSize: 16,
    lineHeight: 20
  },
  loading_container: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 100,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center'
  }
});


export default Accueil
