//This is an example code to show Image Icon in TextInput// 
import React, { Component } from 'react';
//import react in our code.
 
import { StyleSheet, View, TextInput, Image } from 'react-native';
//import all the components we are going to use. 
 
export default class Test extends React.Component {
  render() {
   
    return (
      <View style={styles.container}>
        <View style={styles.SectionStyle}>
         
 
          <TextInput
            style={{ flex: 1 }}
            placeholder="Enter Your Name Here"
            underlineColorAndroid="transparent"
          />
           <Image
            //We are showing the Image from online
            source={{uri:'https://raw.githubusercontent.com/AboutReact/sampleresource/master/input_username.png',}}
 
            //You can also show the image from you project directory like below
            //source={require('./Images/user.png')}
 
            //Image Style
            style={styles.ImageStyle}
          />
        </View>
         <View style={styles.SectionStyle}>
          
 
          <TextInput
            style={{ flex: 1 }}
            placeholder="Enter Your Mobile No Here"
            underlineColorAndroid="transparent"
          />
          <Image
            //We are showing the Image from online
            source={{uri:'https://raw.githubusercontent.com/AboutReact/sampleresource/master/input_phone.png',}}
            
            //You can also show the image from you project directory like below
            //source={require('./Images/phone.png')}
 
            //Image Style
            style={styles.ImageStyle}
          />
        </View>
      </View>
    );
  }
}
 
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    margin: 10,
  },
 
  SectionStyle: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    borderWidth: 0.5,
    borderColor: '#000',
    height: 40,
    borderRadius: 5,
    margin: 10,
  },
 
  ImageStyle: {
    padding: 10,
    margin: 5,
    height: 25,
    width: 25,
    resizeMode: 'stretch',
    alignItems: 'center',
  },
});