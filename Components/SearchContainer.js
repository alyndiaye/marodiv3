import React, { Component } from 'react';
import {
  Text,
  View,
  TouchableOpacity, FlatList, Dimensions, StyleSheet
} from 'react-native';
import SearchBar from 'react-native-searchbar';
import List2Column from './List2Column'

const ITEM_WIDTH = Dimensions.get('window').width
export default class SearchContainer extends Component {

  constructor(props) {
    super(props);
    this.state = {
      films: [],
      columns: 2
    };
    this._handleResults = this._handleResults.bind(this);
  }

  _handleResults() {
    fetch('https://www.marodi.tv/api/accueil')
      .then(response => response.json())
      .then((data) => this.setState({ films: data.videos}));

  }



  render() {
    const {films, columns } = this.state
    return (
      <View  style={{marginTop:50, backgroundColor: 'black'}}>
        <View>
        <FlatList
        numColumns={columns}
        data={films}
        keyExtractor={(item) => item.id.toString()}
        renderItem={({item}) => <List2Column itemWidth={(ITEM_WIDTH-5)/columns} film={item} typeItem="episode"/>}
      />

        </View>
        <SearchBar
          ref={(ref) => this.searchBar = ref}
          data={films}
          handleResults={this._handleResults}
          showOnLoad
          cancelButtonTitle='Cancel'
          platform="android"
        />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  default_text: {
    marginLeft: 5,
    marginRight: 5,
    marginTop: 20,
    color: 'white',
    fontSize: 20
  },
  loading_container: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 100,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center'
  }
});
