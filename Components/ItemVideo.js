
import React from 'react'
import { StyleSheet, Text, TouchableOpacity, Image, View } from 'react-native'

class ItemVideo extends React.Component {

  render() {
    //console.log(this.props.isFilmFavorite);
    const {video ,displayFilm} = this.props
    //console.log(detailvideo)
    return (
      <TouchableOpacity
      onPress={()=> displayFilm(video)}
      style={styles.main_container} >
        <Image
          style={styles.image}
          source={{uri: video.url_vignette}}
        />
        <View style={styles.content_container}>
          <View style={styles.header_container}>
            <Text style={styles.title_text}>{video.name}</Text>
          </View>
          <View style={styles.status_container}>
            <Text style={styles.status_text}>gratuit</Text>
          </View>
          <View style={styles.description_container}>
            <Text style={styles.description_text} numberOfLines={4}>{video.description}</Text>
            {/* La propriété numberOfLines permet de couper un texte si celui-ci est trop long, il suffit de définir un nombre maximum de ligne */}
          </View>
          
        </View>
        
      </TouchableOpacity>
    )
  }
}

const styles = StyleSheet.create({
  main_container: {
    height: 200,
    flexDirection: 'row'
  },
  image: {
    width: 120,
    height: 180,
    margin: 5,
    backgroundColor: 'gray'
  },
  content_container: {
    flex: 1,
    margin: 5
  },
  header_container: {
    flex: 2,
    flexDirection: 'row'
  },
  status_container: {
    flex: 1
  },
  title_text: {
    fontWeight: 'bold',
    fontSize: 20,
    flex: 1,
    flexWrap: 'wrap',
    paddingRight: 5,
    color: 'white'
  },
  status_text:{
    fontSize: 10,
    color: 'green'
  },
  saison_text:{
    fontSize: 11,
    color: '#666666'
  },
  vote_text: {
    fontWeight: 'bold',
    fontSize: 26,
    color: '#666666'
  },
  description_container: {
    flex: 5
  },
  saison_container: {
    flex: 1
  },
  description_text: {
    fontStyle: 'italic',
    color: '#666666'
  },
  vm_container: {
    flex: 1
  },
  vm_text: {
    textAlign: 'left',
    fontSize: 12,
    color: '#F1B331'
  },
  favorite_image: {
    width: 25,
    height: 25,
    marginRight: 5
  }
})


export default ItemVideo
