import React, { Component } from 'react';
import { Image } from 'react-native';
import { PropTypes } from 'prop-types';
import Icon from 'react-native-vector-icons/FontAwesome';
import {
  View,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';

export default class HeartButton extends Component {
  constructor(props) {
  	super(props);
  	
  }

  componentWillReceiveProps(nextProps) {
    this.setState({ addedToFavorite: nextProps.selected });
  }



  render() {
  	//const { addedToFavorite } = this.state;
  	//const { color, selectedColor, onPress } = this.props;

    return (
   
        <View style={{justifyContent: 'center', alignItems: 'center'}}>
          <Image 
            source={require('../assets/ic_favorite.png')}
            //color={addedToFavorite ? 'white' : 'blue'}
            size={18}
            style={{width:56, height:56}}
          />

          
        </View>
     
    );
  }
}



const styles = StyleSheet.create({
  selectedColor: {
    position: 'absolute',
    left: 0,
    top: 0,
  },
});