
import React from 'react'
import { StyleSheet, Text, TouchableOpacity, Image, View } from 'react-native'

class ItemSerie extends React.Component {

  render() {
    //console.log(this.props.isFilmFavorite);
    const {serie,detailSerie} = this.props
    //console.log(detailSerie)
    return (
      <TouchableOpacity
      onPress={()=> detailSerie(serie)}
      style={styles.main_container} >
      <View style={{ flex: 1, flexDirection: 'row', marginTop:19 }}>
      
      <Image
      style={{ height:163, width:113, marginLeft:16 }}
      source={{uri: serie.url_vignette}}
      />
      
      <View style={styles.text_container}>
      <Text style={styles.text_name}>{serie.name}</Text>
      <Text style={styles.content_type}>Gratuit</Text>
      <Text style={styles.text_description} numberOfLines={3}>{serie.description}</Text>
      <Text style={styles.text_nbs} >{serie.seasons_number} saison(s)</Text>
      <Text style={styles.text_vp}>voir les episodes</Text>

      </View>
      
      </View>
      
      
      </TouchableOpacity>
      )
  }
}

const styles = StyleSheet.create({
  main_container: {
    flexDirection: 'row',
    backgroundColor: '#1C1C1C'
  },
  text_container:{
   height:163, 
   flex:1, 
   marginLeft:13, 
   marginRight:16 
 },
 image: {
  width: null,
  height: null
},
text_name:{
  marginTop:10, 
  color: '#FFFFFF', 
  fontSize: 16, 
  lineHeight:20,
  fontWeight: '500',
},
content_type:{
  marginTop:1, 
  color: '#18F68D', 
  fontSize:12, 
  lineHeight:15,
  fontWeight: '500'
},
text_description:{
  marginTop:11, 
  color: '#8E8E8E', 
  fontSize:12, 
  lineHeight:16
},
text_nbs:{
  marginTop:3, 
  color: '#8E8E8E', 
  fontSize:10, 
  lineHeight:16
},
text_vp:{
  marginTop:16, 
  color: '#F8AF16', 
  fontSize:11, 
  lineHeight:14,
  fontWeight: '300'
}

})

export default ItemSerie
