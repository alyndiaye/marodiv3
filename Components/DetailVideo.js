
import React from 'react'
import SvgUri from 'react-native-svg-uri';
//import SVGImage from 'react-native-svg-image';
import { 
	StyleSheet, 
	Text, 
	View, 
	Dimensions, 
	Animated,
	Platform,
	StatusBar,
	RefreshControl,
	TouchableOpacity,
	FlatList,
	//Image
} from 'react-native'
import Dialog, {
	DialogTitle,
	DialogContent,
	DialogFooter,
	DialogButton,
	SlideAnimation,
	ScaleAnimation,
} from 'react-native-popup-dialog';
import Image from 'react-native-remote-svg'
import List2Column from './List2Column'
import { connect } from 'react-redux'


const HEADER_MAX_HEIGHT = 200;
const HEADER_MIN_HEIGHT = Platform.OS === 'ios' ? 60 : 73;
const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;
const ITEM_WIDTH = Dimensions.get('window').width

class DetailVideo extends React.Component {
	static navigationOptions = {
		headerTransparent: true
	}
	constructor(props) {
		super(props);

		this.state = {
			defaultAnimationDialog: false,
			films: [],
			columns: 2,
			scrollY: new Animated.Value(
        // iOS has negative initial scroll value because content inset...
        Platform.OS === 'ios' ? -HEADER_MAX_HEIGHT : 0,
        ),
			refreshing: false,
		};
	}

	_displayFilm= (video) => {
		this.props.navigation.navigate('Player', {video: video})
	}

	componentDidMount() {
		this.setState({ isLoading: true })
		fetch('https://www.marodi.tv/api/suggerer/'+this.props.navigation.state.params.video.slug)
		.then(response => response.json())
		.then(data => this.setState({
			films: data.videos,
		}));
	}
	 _toggleFavorite() {
        // Définition de notre action ici
        //console.log("favorie")
        const action = {type: "TOGGLE_FAVORITE", value: this.props.navigation.state.params.video}
        this.props.dispatch(action)
    }

    _displayFavoriteImage() {
    var sourceImage = require('../assets/ic_favorite_border.png')
    if (this.props.favoritesFilm.findIndex(item => item.id === this.props.navigation.state.params.video.id) !== -1) {
      // Film dans nos favoris
      sourceImage = require('../assets/ic_favorite.png')
    }
    return (
      <Image
        style={styles.imgStyle}
        source={sourceImage}
      />
    )
}
  //   componentDidUpdate() {
  //   console.log("componentDidUpdate : ")
  //   console.log(this.props.favoritesFilm)
  // }
	render() {
		const {films,columns} = this.state
		const {video} = this.props.navigation.state.params
		const scrollY = Animated.add(
			this.state.scrollY,
			Platform.OS === 'ios' ? HEADER_MAX_HEIGHT : 0,
			);
		const headerTranslate = scrollY.interpolate({
			inputRange: [0, HEADER_SCROLL_DISTANCE],
			outputRange: [0, -HEADER_SCROLL_DISTANCE],
			extrapolate: 'clamp',
		});

		const imageOpacity = scrollY.interpolate({
			inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
			outputRange: [1, 1, 0],
			extrapolate: 'clamp',
		});
		const imageTranslate = scrollY.interpolate({
			inputRange: [0, HEADER_SCROLL_DISTANCE],
			outputRange: [0, 100],
			extrapolate: 'clamp',
		});

		const titleScale = scrollY.interpolate({
			inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
			outputRange: [1, 1, 0.8],
			extrapolate: 'clamp',
		});
		const titleTranslate = scrollY.interpolate({
			inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
			outputRange: [0, 0, -8],
			extrapolate: 'clamp',
		});
		//console.log(this.props.favoritesFilm)
		return (
			<View style={styles.fill}>
			<StatusBar
			translucent
			barStyle="light-content"
			backgroundColor="rgba(0, 0, 0, 0.251)"
			/>
			<Animated.ScrollView
			style={styles.fill}
			scrollEventThrottle={1}
			onScroll={Animated.event(
				[{ nativeEvent: { contentOffset: { y: this.state.scrollY } } }],
				{ useNativeDriver: true },
				)}
			refreshControl={
				<RefreshControl
				refreshing={this.state.refreshing}
				onRefresh={() => {
					this.setState({ refreshing: true });
					setTimeout(() => this.setState({ refreshing: false }), 1000);
				}}
              // Android offset for RefreshControl
              progressViewOffset={HEADER_MAX_HEIGHT}
              />
          }
          // iOS offset for RefreshControl
          contentInset={{
          	top: HEADER_MAX_HEIGHT,
          }}
          contentOffset={{
          	y: -HEADER_MAX_HEIGHT,
          }}
          >
          <View style={styles.stateContainer}>
          <Text style={styles.stateStyle}>
          {video.sous_titre ? 'wolof' : 'wolof'} {video.sous_titre ? ' | VOSTFR' : ''} {video.duration}
          </Text>
          <Text style={styles.statuStyle}> 
          {video.price === 0 ? 'Gratuit' : video.price}
          </Text>

          </View>
          <View style={styles.title_container}>
          <View style={styles.titleContainer}>
          <Text style={styles.titleStyle}>
          {video.name}
          </Text>

          </View>
          <TouchableOpacity onPress={() => {
          	this.setState({
          		defaultAnimationDialog: true,
          	});
          }}>
          

          <Image source={require('../assets/warning.svg')} style={{height:24, width:24}} />
          </TouchableOpacity>
          <Dialog
          onDismiss={() => {
          	this.setState({ defaultAnimationDialog: false });
          }}
          width={0.9}
          visible={this.state.defaultAnimationDialog}
          rounded
          actionsBordered
          dialogTitle={
          	<View style={{backgroundColor: '#212121', alignItems: 'center', justifyContent: 'center', height:100}}>


          	<
          	Image source={require('../assets/warning.svg')} style={{width:50, height:50}}
          	/>
          	</View>
          }
          footer={

          	<DialogButton
          	text="OK"
          	bordered
          	onPress={() => {
          		this.setState({ defaultAnimationDialog: false });
          	}}
          	key="button-1"
          	/>

          }>
          <DialogContent
          style={{
          	backgroundColor: '#F7F7F8',
          }}>
          <Text>Synopsis:</Text>
          <Text>{video.description}</Text>
          </DialogContent>
          </Dialog>

          </View>
          <View style={styles.social_container}>
          <View style={styles.favorie_container}>
          <TouchableOpacity onPress={()=>this._toggleFavorite()}>
          {this._displayFavoriteImage()}
          </TouchableOpacity>
          <Text style={styles.labelStyle}>Ajouter à mes favorie</Text>
          </View>
          <View style={styles.share_container}>
          <Image 
          source={require('../assets/shared.svg')}

          size={18}
          style={styles.imgStyle}
          />
          <Text style={styles.labelStyle}>Partager</Text>

          </View>
          <View style={styles.cast_container}>
          <Image 
          source={require('../assets/cast.svg')}

          size={18}
          style={styles.imgStyle}
          />
          <Text style={styles.labelStyle}>Chrome cast</Text>

          </View>

          </View>
          <View style={styles.bying_container}>
          <View style={styles.baContainer}>
          <Image

          source={
          	require('../assets/play.svg')

          }
          style={styles.ImageIconStyle}
          />
          <Text style={styles.labelButtonStyle}> Regarder </Text>
          </View>
          <View style={styles.acheterContainer}>
          <Image
		  source={require('../assets/panier.svg')}
          style={styles.ImageIconStyle}
          />
          <Text style={styles.labelButtonStyle}> Acheter </Text>
          </View>

          </View>
          <View style={styles.realContainer}>
          <Text>
          <Text style={styles.title_text} >Production     </Text>
          <Text style={styles.value_text} >{video.producteur.name}</Text>
          </Text>

          </View>

          <View style={styles.suggererContainer}>
          <Text style={styles.SuggererStyle}>
          Vidéos suggerées
          </Text>
          <View style={{flex: 1}}>
          <FlatList
          numColumns={columns}
          data={films}
          keyExtractor={(item) => item.id}
          renderItem={({item}) => 
          <List2Column itemWidth={(ITEM_WIDTH-20)/columns} film={item}
          displayFilm={this._displayFilm}
          navigation={this.props.navigation}
          />

      }
      />



      </View>

      </View>
      </Animated.ScrollView>
      <Animated.View
      pointerEvents="none"
      style={[
      	styles.header,
      	{ transform: [{ translateY: headerTranslate }] },
      	]}
      	>
      	<Animated.Image
      	style={[
      		styles.backgroundImage,
      		{
      			opacity: imageOpacity,
      			transform: [{ translateY: imageTranslate }],
      		},
      		]}
      		source={{uri: video.url_vignette}}
      		>

      		</Animated.Image>

      		</Animated.View>
      		<Animated.View
      		style={[
      			styles.bar,
      			{
      				transform: [
      				{ scale: titleScale },
      				{ translateY: titleTranslate },
      				],
      			},
      			]}
      			>
      			<Text style={styles.title}>Title</Text>
      			</Animated.View>
      			</View>
      			)
}
}

const styles = StyleSheet.create({

	main_container: {
		flex:1,
		backgroundColor: '#191919'
	},
	title_container:{
		flexDirection: 'row',
			//marginTop:11,
			marginLeft:16,
			marginRight:16,
		},
		titleStyle:{
			color: '#FFFFFFFF',
			fontSize:18,
			fontWeight: '500',
			lineHeight: 28,
		},
		statuStyle:{
			color: '#1DB954',
		},
		titleContainer:{
			flexWrap: 'wrap',
			flex:1

		},
		imageStyle:{

		},
		social_container:{
			marginTop: 7,
			//height:67,
			flexDirection: 'row',
			flex: 1,
			
		},
		favorie_container:{
			flex:1,
			alignItems: 'center',
			justifyContent: 'center',
			height:67,

		},
		share_container:{
			flex:1,
			alignItems: 'center',
			justifyContent: 'center',
			height:67,

		},
		cast_container:{
			flex:1,
			alignItems: 'center',
			justifyContent: 'center',
			height:67,

		},
		labelStyle:{
			color: '#B4B4B4',
			fontWeight: '500',
			fontSize:9,
			lineHeight: 11,
			marginTop:8,


		},
		imgStyle:{
			width:24, 
			height:24,
			marginTop:10,


		},
		bying_container:{
			flexDirection: 'row',
			flex:1,
			marginLeft:16,
			marginRight:16,
			height:38,
			marginTop: 14,
		},
		baContainer:{
			flexDirection: 'row',
			backgroundColor: '#1DB954',
			borderRadius: 2,
			width: (ITEM_WIDTH-48)/2,
			alignItems: 'center'
		},
		acheterContainer:{
			flexDirection: 'row',
			backgroundColor: '#F1B331',
			borderRadius: 2,
			width: (ITEM_WIDTH-48)/2,
			alignItems: 'center',

			marginLeft:16,

		},
		labelButtonStyle:{
			flex:1,
			color: '#222222',
			fontSize: 12,
			fontWeight: '500',
			lineHeight: 15,
			textAlign: 'center'
		},
		realContainer:{
			flex: 1,
			marginLeft:16,
			marginTop: 28
		},
		title_text: {
			fontSize: 11,
			color: '#9C9C9C',
			lineHeight: 14,

		},
		value_text: {
			fontSize: 11, 
			color: '#FFFFFF',
			lineHeight: 14, 
			fontWeight: 'bold'

		},
		/***


		******/
		fill: {
			flex: 1,
			backgroundColor: '#191919'
		},
		content: {
			flex: 1,
		},
		header: {
			position: 'absolute',
			top: 0,
			left: 0,
			right: 0,
			backgroundColor: '#212121',
			overflow: 'hidden',
			height: HEADER_MAX_HEIGHT,
		},
		backgroundImage: {
			position: 'absolute',
			top: 0,
			left: 0,
			right: 0,
			width: null,
			height: HEADER_MAX_HEIGHT,
			resizeMode: 'cover',
		},
		bar: {
			backgroundColor: 'transparent',
			marginTop: Platform.OS === 'ios' ? 28 : 38,
			height: 32,
			alignItems: 'center',
			justifyContent: 'center',
			position: 'absolute',
			top: 0,
			left: 0,
			right: 0,
		},
		title: {
			color: 'white',
			fontSize: 18,
		},
		scrollViewContent: {
    // iOS uses content inset, which acts like padding.
    paddingTop: Platform.OS !== 'ios' ? HEADER_MAX_HEIGHT : 0,
},
row: {
	height: 40,
	margin: 16,
	backgroundColor: '#D3D3D3',
	alignItems: 'center',
	justifyContent: 'center',
},
styleButton: {
	flexDirection: 'row',
	alignItems: 'center',
	justifyContent: 'center',
	backgroundColor: '#1DB954',
	height: 38,
	width: (ITEM_WIDTH-48)/2,
	borderRadius: 2,
	marginTop:10,
},
styleButtonBy: {
	flexDirection: 'row',
	alignItems: 'center',
	justifyContent: 'center',
	backgroundColor: '#F1B331',
	height: 38,
	width: (ITEM_WIDTH-48)/2,
	borderRadius: 2,
	marginTop:10,
	marginLeft:16,
},
ImageIconStyle: {
	
	height: 20,
	width: 20,
},
TextStyle: {
	flex:1,
	color: '#222222',
	fontSize:12, 
	textAlign: 'center',
	lineHeight:15,
	fontWeight: 'bold'
},
stateContainer:{
	flex:1,
	flexDirection: 'row',
	marginTop:205,
	marginLeft:16,
	marginRight:16
},
stateStyle:{
	color: '#F1B331',
	flexWrap: 'wrap',
	flex: 1
},
suggererContainer:{

	marginTop:30
},
SuggererStyle:{
	color: '#EFEFEF',
	fontSize: 18,
	fontWeight: 'bold',
	lineHeight: 23,
	marginLeft:16
}
})

const mapStateToProps = (state) => {
  return {
    favoritesFilm: state.favoritesFilm
  }
}

export default connect(mapStateToProps)(DetailVideo)