
import React from 'react'
import { 
	StyleSheet, 
	Text, 
	View, 
	Image,
	Dimensions, 
 	Animated,
  	Platform,
  	StatusBar,
  	RefreshControl,
  	TouchableOpacity,
  	ScrollView,
  	ImageBackground


} from 'react-native'


const HEADER_MAX_HEIGHT = 121;
const HEADER_MIN_HEIGHT = Platform.OS === 'ios' ? 60 : 73;
const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;

class DetailVideo extends React.Component {
	static navigationOptions = {
		headerTransparent: true
	}
	constructor(props) {
    super(props);

    this.state = {
    	seasons: [],
      scrollY: new Animated.Value(
        // iOS has negative initial scroll value because content inset...
        Platform.OS === 'ios' ? -HEADER_MAX_HEIGHT : 0,
      ),
      refreshing: false,
    };
  }

  componentDidMount() {
		fetch('https://www.marodi.tv/api/serie/'+this.props.navigation.state.params.serie.slug+'?include=seasons')
		.then(response => response.json())
		.then(data => this.setState({
			seasons: data.seasons.data
		}));
	}

	_displayFilm(item){
		this.props.navigation.navigate('Player', {video: item})
	}

 

	render() {
		const {video} = this.props.navigation.state.params
		   const scrollY = Animated.add(
      this.state.scrollY,
      Platform.OS === 'ios' ? HEADER_MAX_HEIGHT : 0,
    );
    const headerTranslate = scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE],
      outputRange: [0, -HEADER_SCROLL_DISTANCE],
      extrapolate: 'clamp',
    });

    const imageOpacity = scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
      outputRange: [1, 1, 0],
      extrapolate: 'clamp',
    });
    const imageTranslate = scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE],
      outputRange: [0, 100],
      extrapolate: 'clamp',
    });

    const titleScale = scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
      outputRange: [1, 1, 0.8],
      extrapolate: 'clamp',
    });
    const titleTranslate = scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
      outputRange: [0, 0, -8],
      extrapolate: 'clamp',
    });
		const {seasons} = this.state
		const {serie} = this.props.navigation.state.params
		return (
			<View style={styles.fill}>
        <StatusBar
          translucent
          barStyle="light-content"
          backgroundColor="rgba(0, 0, 0, 0.251)"
        />
        <Animated.ScrollView
          style={styles.fill}
          scrollEventThrottle={1}
          onScroll={Animated.event(
            [{ nativeEvent: { contentOffset: { y: this.state.scrollY } } }],
            { useNativeDriver: true },
          )}
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={() => {
                this.setState({ refreshing: true });
                setTimeout(() => this.setState({ refreshing: false }), 1000);
              }}
              // Android offset for RefreshControl
              progressViewOffset={HEADER_MAX_HEIGHT}
            />
          }
          // iOS offset for RefreshControl
          contentInset={{
            top: HEADER_MAX_HEIGHT,
          }}
          contentOffset={{
            y: -HEADER_MAX_HEIGHT,
          }}
        >
          <View style={styles.container_img_txt}>
			<Image
			style={{ height:117, width:85 }}
			source={{uri: serie.url_vignette}}
			/>
			<View style={styles.container_nba}>
			<Text numberOfLines={1} style={styles.text_name}>{serie.name}</Text>
			<TouchableOpacity style={styles.styleButton} activeOpacity={0.5}>
			<Image
            //We are showing the Image from online
            source={
            	require('../assets/play.png')
            	
        }
            style={styles.ImageIconStyle}
          />
          <Text style={styles.TextStyle}> Regaerder la bande d'annone </Text>
          </TouchableOpacity>
			</View>
			</View>
			<View style={styles.container_description}>
			<Text style={styles.description_text} >{serie.description}</Text>
			<Text style={styles.title_text_containers}>
				<Text style={styles.title_text} >Production      </Text>
				<Text style={styles.value_text} >{serie.producteur.name}</Text>
			</Text>
			<Text style={styles.title_text_container}>
				<Text style={styles.title_text} >Realisation     </Text>
				<Text style={styles.value_text} >{serie.realisateur}</Text>
			</Text>
			<Text style={styles.title_text_container}>
				<Text style={styles.title_text} >Langue           </Text>
				<Text style={styles.value_text} >{serie.langue.name}</Text>
			</Text>
			<Text style={styles.title_text_container}>
				<Text style={styles.title_text} >Année             </Text>
				<Text style={styles.value_text} >{serie.annee}</Text>
			</Text>
			</View>
			
			<View 
				style={{marginTop:31}} >
			{ seasons.map((item, key)=>(
				<ScrollView
				key={key}
				scrollEventThrottle={16}

				> 
				<View style={{ flex: 1, marginLeft:16}}>
				<Text >
	 				<Text style={{ fontSize: 18, color: 'white', lineHeight:23, fontWeight: 'bold', marginRight: 10 }}>
						{item.name+' '}      
					</Text>
					<Text style={{color: '#B4B4B4', fontSize:12, lineHeight:15}}>
						{' '+item.videos.data.length} épisodes
					</Text>
				</Text>

				<View style={{ height: 80, marginTop: 20 }}>
				<ScrollView
				horizontal={true}
				showsHorizontalScrollIndicator={false}
				>
				{ item.videos.data.map((item, key)=>(
					<ImageBackground key={key} style={{borderRadius: 2, width:109, height:51, backgroundColor: '#212121', marginRight:15,
					flex: 1,
					alignItems: 'center',
					justifyContent: 'center'}} > 
					<Text style={styles.episode_text} onPress={()=> this._displayFilm(item)} >{item.name.split('- ')[1]}</Text>
					</ImageBackground>)
					)}
					</ScrollView>
					</View>
					</View>
					</ScrollView>
					)
					)}
					</View>
        </Animated.ScrollView>
        <Animated.View
          pointerEvents="none"
          style={[
            styles.header,
            { transform: [{ translateY: headerTranslate }] },
          ]}
        >
          <Animated.Image
            style={[
              styles.backgroundImage,
              {
                opacity: imageOpacity,
                transform: [{ translateY: imageTranslate }],
              },
            ]}
            source={{uri: serie.cover}}
          />
        </Animated.View>
        <Animated.View
          style={[
            styles.bar,
            {
              transform: [
                { scale: titleScale },
                { translateY: titleTranslate },
              ],
            },
          ]}
        >
          <Text style={styles.title}></Text>
        </Animated.View>
      </View>
			)
		}
	}

	const styles = StyleSheet.create({

		main_container: {
			flex:1,
			backgroundColor: 'black'
		},
		title_container:{
			flexDirection: 'row',
			marginTop:216,
			marginLeft:16,
			marginRight:16,
		},
		titleStyle:{
			color: '#FFFFFFFF',
			fontSize:18,
			fontWeight: '500',
			lineHeight: 28,
			flexWrap: 'wrap',
			flex:1
		},
		imageStyle:{

		},
		social_container:{
			marginTop: 7,
			//height:67,
			flexDirection: 'row',
			flex: 1,
			
		},
		favorie_container:{
			flex:1,
			alignItems: 'center',
    		justifyContent: 'center',
			height:67,

		},
		share_container:{
			flex:1,
			alignItems: 'center',
    		justifyContent: 'center',
			height:67,

		},
		cast_container:{
			flex:1,
			alignItems: 'center',
    		justifyContent: 'center',
			height:67,

		},
		labelStyle:{
			color: '#B4B4B4',
			fontWeight: '500',
			fontSize:9,
			lineHeight: 11,
			marginTop:8,


		},
		imgStyle:{
			width:24, 
			height:24,
			marginTop:10

		},
		bying_container:{
			flexDirection: 'row',
			marginLeft:16,
			marginRight:16,
			height:38
		},
		/***


		******/
		 fill: {
    flex: 1,
    backgroundColor: '#191919'
  },
  content: {
    flex: 1,
  },
  header: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    backgroundColor: '#212121',
    overflow: 'hidden',
    height: HEADER_MAX_HEIGHT,
  },
  backgroundImage: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    width: null,
    height: HEADER_MAX_HEIGHT,
    resizeMode: 'cover',
  },
  bar: {
    backgroundColor: 'transparent',
    marginTop: Platform.OS === 'ios' ? 28 : 38,
    height: 32,
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
  },
  title: {
    color: 'white',
    fontSize: 18,
  },
  scrollViewContent: {
    // iOS uses content inset, which acts like padding.
    paddingTop: Platform.OS !== 'ios' ? HEADER_MAX_HEIGHT : 0,
  },
  row: {
    height: 40,
    margin: 16,
    backgroundColor: '#D3D3D3',
    alignItems: 'center',
    justifyContent: 'center',
  },
  container_nba:{
		height:117, 
		flex:1, 
		marginLeft:9, 
		marginRight:16 
	},
	title_text_containers:{
		marginTop: 20,
			marginLeft: 16
	},
	title_text_container:{
		marginTop: 12,
			marginLeft: 16
	},
	text_name:{
		marginLeft: 9,
		marginTop:41 ,
		color: '#FFFFFF',
		fontSize: 24, 
		lineHeight:28,
		height: 28
	},
	bgImage:{
		width: null,
		height: 121

	},
	container_img_txt: {
		flexDirection: 'row',
		marginLeft:16, 
		marginTop: 86,
		height: 117
	},
	container_description:{

	},
	description_text: {
		color: '#B4B4B4',
		fontSize: 12,
		lineHeight: 16,
		marginLeft: 16 ,
		marginRight: 16,
		marginTop: 15
	},
	product_container: {
		height:60,
	},
	title_text: {
		fontSize: 11,
		color: '#9C9C9C',
		lineHeight: 14,

	},
	value_text: {
		fontSize: 11, 
		color: '#FFFFFF',
		lineHeight: 14, 
		fontWeight: 'bold'

	},
	episode_text:{
		color: '#D0D0D0',
		fontSize: 12,
		lineHeight: 15,
		fontWeight: 'bold'
	},
	containter: {
    width: Dimensions.get("window").width, //for full screen
    height: 121,//Dimensions.get("window").height //for full screen
},
fixed: {
	position: "absolute",
	top: 0,
	left: 0,
	right: 0,
	bottom: 0
},
scrollview: {
	backgroundColor: 'black'
},
ImageIconStyle: {
     padding: 10,
    margin: 5,
    height: 20,
    width: 20,
    resizeMode: 'stretch',
  },
  TextStyle: {
  	flex:1,
    color: '#222222',
    fontSize:12, 
    textAlign: 'center',
    lineHeight:15,
    fontWeight: 'bold'
  },
  styleButton: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#1DB954',
    height: 38,
    width: 213,
    borderRadius: 2,
    marginLeft:9,
    marginTop:10,
  },
	})

	export default DetailVideo