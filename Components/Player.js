import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { WebView } from 'react-native-webview';

// ...
class Player extends React.Component {
  static navigationOptions = {
    header: null,
    tabBarVisible: false
  }
  render() {
    return (
      <View style={{flex: 1}} >

      <WebView source={{ uri: 'https://www.marodi.tv/webview/'+this.props.navigation.state.params.video.slug }} />
      </View>
    
    );
  }
}

export default Player