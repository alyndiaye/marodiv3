import React from 'react'
import {View, Image, ImageBackground, Text, StyleSheet, TouchableOpacity} from 'react-native'
import PlayerButton from './PlayerButton'

class AfficheItem extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      film: {}
    }
  }
  componentDidMount(){
    fetch('https://www.marodi.tv/api/panaroma')
    .then(response => response.json())
    .then(data => this.setState({ film: data }));
  }

  render(){
    const {displayFilm} = this.props
    return(
      <TouchableOpacity onPress={()=> displayFilm(this.state.film)}>
      
        <ImageBackground source={{uri: this.state.film.url_affiche}} style={styles.bgImage}  >
      <View style={{justifyContent: 'center', alignItems: 'center', marginTop: 65}} >
           
            <PlayerButton
                color={'#ffffff'}
                selectedColor={'#fc4c54'}
                //selected={favouriteListings.indexOf(listing.id) > -1}
                onPress={this.showMenu}
              />
              <Text style={styles.afficheStyle}>
              Affiche
              </Text>
              <Text style={styles.nameStyle}>
              {this.state.film.name}
              </Text>


      </View>
         
        </ImageBackground>
      </TouchableOpacity>
    )
  }
}

const styles = StyleSheet.create({
  container:{
  },
  bgImage:{
    width: null,
    height: 202
  },
  textOver:{
    fontWeight: 'bold',
    color: 'white',
    position: 'absolute', // child
    bottom: 100, // position where you want
    left: 0,
    textAlign: 'center'
  },
  afficheStyle:{
color: '#FFFFFF',
fontSize: 10,
lineHeight: 11,
marginTop: 12
  },
  nameStyle:{
color: '#FFFFFF',
fontSize: 12,
lineHeight: 14,
marginTop:6
  }

})

export default AfficheItem
