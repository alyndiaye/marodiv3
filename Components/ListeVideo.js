
import React from 'react'
import { StyleSheet, Text, FlatList, Dimensions,View } from 'react-native'
import ItemVideo from './ItemVideo'
import List2Column from './List2Column'
const ITEM_WIDTH = Dimensions.get('window').width

class ListeVideo extends React.Component {
	static navigationOptions = ({ navigation }) => {
    return {
      title: navigation.state.params.genre,
      headerStyle: {
      backgroundColor: global.bgcNavBar,
    },
    headerTintColor: '#fff',
    headerTitleStyle: {
      fontWeight: 'bold',
      color: 'white'
    },
    };
  }
  constructor(props) {
    super(props);
  
    this.state = {
    	videos:[],
      columns: 2,
      typeItem: 'episode'
    };
  }

  componentDidMount() {
    fetch('https://www.marodi.tv/api/categorie/'+this.props.navigation.state.params.slug)
		.then(response => response.json())
		.then(data => this.setState({
			videos: data.videos
		}));
  }
  _displayFilm= (video) => {
    this.props.navigation.navigate('Player', {video: video})
  }

  render() {
  	const {videos, columns, typeItem} = this.state
  	//console.log(this.state.videos)
    return (
      <View style={{flex:1, backgroundColor:'black' }}>
      <FlatList 
      numColumns={columns}
      data={videos}
			keyExtractor={(item) => item.id.toString()}
			renderItem={({item}) => 
<List2Column itemWidth={(ITEM_WIDTH-20)/columns} film={item} typeItem={typeItem}
               displayFilm={this._displayFilm}
               />
    }
			/>
      </View>
    )
  }
}

const styles = StyleSheet.create({})

export default ListeVideo
