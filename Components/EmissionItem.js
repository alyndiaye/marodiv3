import React from 'react'
import {View, Image, ImageBackground, Text, StyleSheet} from 'react-native'

class EmissionItem extends React.Component {
  constructor(props) {
    super(props);
    this.state = {

    }
  }
//'https://image.tmdb.org/t/p/w300'+ this.props.film.poster_path
  render(){
    //console.log(this.props);
    const {itemWidth} = this.props
    return(
      <View style={styles.container}>
        <Image  source={{uri: this.props.film.url_vignette}} style={{width: itemWidth, height:300, margin:5}}  >
        </Image>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container:{

  },
  bgImage:{
    width: null,
    height: 100
  },
  textOver:{
    fontWeight: 'bold',
    color: 'white',
    position: 'absolute', // child
    bottom: 100, // position where you want
    left: 0,
    textAlign: 'center'
  }

})

export default EmissionItem
