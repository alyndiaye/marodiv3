import React from 'react'
import {View, Image, ImageBackground, Text, StyleSheet, TouchableOpacity, Share} from 'react-native'
import Menu, { MenuItem, MenuDivider } from 'react-native-material-menu';
import MenuButton from './MenuButton'
//import HeartButton from './HeartButton'
class List2Column extends React.Component {
  constructor(props) {
    super(props);
    this.state = {

    }
  }
  _menu = null;

  setMenuRef = ref => {
    this._menu = ref;
  };

  hideMenu = () => {
    this._menu.hide();
  };

  showMenu = () => {
    this._menu.show();
  };
  lire = () => {
    this._menu.hide();
    this.props.navigation.navigate('Player',{video: this.props.film})
  };
  partager = () => {
    this._menu.hide();
    Share.share({ message: 'marodi.tv/player/'+this.props.film.slug, title : "Sharing via react native" })
    //after successful share return result
    .then(result => console.log(result))
    //If any thing goes wrong it comes here
    .catch(errorMsg => console.log(errorMsg));
  };
  caster = () => {
    this._menu.hide();
  };
  detail = () => {
    this._menu.hide();
    this.props.navigation.navigate('DetailVideo',{video: this.props.film})
  };


  _onPressButton(){
    //console.log(this.props.displayFilm)
  }
//'https://image.tmdb.org/t/p/w300'+ this.props.film.poster_path
render(){
  const {film, itemWidth, typeItem, displayFilm} = this.props
    //console.log(film.serie);

    //if (typeItem === "episode") {

      return(
        <TouchableOpacity onPress={()=> displayFilm(film)} style={{margin:5, borderRadius:2, marginBottom: 16}}>
        <View style={styles.addToFavoriteBtn}>

        <Menu
        ref={this.setMenuRef}
        button={<MenuButton
          color={'#ffffff'}
          selectedColor={'#fc4c54'}
                //selected={favouriteListings.indexOf(listing.id) > -1}
                onPress={this.showMenu}
                />}
                >
                <MenuItem onPress={this.lire}>Lire</MenuItem>
                <MenuItem onPress={this.partager}>Partager</MenuItem>
                <MenuItem onPress={this.caster}>
                Caster
                </MenuItem>
                <MenuItem onPress={this.detail}>Detail</MenuItem>
                </Menu>
                </View>
                <Image
                style={{flex:1, height:88, width: itemWidth}}
                source={{uri: this.props.film.url_affiche}}
                />
                <View style={{flexDirection: 'row', width: itemWidth}}>
                <View style={{ flexDirection: 'row',
                alignItems: 'center',flex:1 }}>
                <Text numberOfLines={1} style={{color: 'white', marginTop: 8, fontSize: 13, color: '#FDFDFD', flexWrap: 'wrap',
                flex: 1, lineHeight: 16}}>
                {film.name}
                </Text>
                <View>

                </View>
                </View>
                <View >
                </View>
                </View>
                <View style={{width: itemWidth, flexDirection: 'row', marginTop: 1}}>

                <Text style={styles.title_text}>
                { film.cat==="Series" ? film.serie.name : film.categorie.name}

                </Text>
                <Text style={styles.vote_text}>
                {film.price !== 0 ? film.price : 'Gratuit'}
                </Text>
                </View>
                </TouchableOpacity>
                )
              }
            }

            const styles = StyleSheet.create({
              container:{

              },
              bgImage:{
                width: null,
                height: 100
              },
              textOver:{
                fontWeight: 'bold',
                color: 'white',
                position: 'absolute', // child
                bottom: 100, // position where you want
                left: 0,
                textAlign: 'center'
              },
              title_text: {
                fontSize: 11,
                color: '#7A7A7A',
                flexWrap: 'wrap',
                flex: 1,
                //paddingRight: 5,
                // marginTop: 16
              },
              vote_text: {
                fontWeight: 'bold',
                fontSize: 11,
                color: '#18F68D',


              },
              imageStyle:{
                width:16,
                height:16,
                //marginLeft: 50

              },
              addToFavoriteBtn: {
                position: 'absolute',
                right: 12,
                top: 7,
                zIndex: 2,
              },

            })

            export default List2Column
