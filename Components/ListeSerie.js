
import React from 'react'
import { StyleSheet, Text, FlatList, View } from 'react-native'
import ItemSerie from './ItemSerie'

class ListeSerie extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			series: []
		};
	}
	componentDidMount() {
		fetch('https://www.marodi.tv/api/series')
		.then(response => response.json())
		.then(data => this.setState({
			series: data.series
		}));

	}

	_detailSerie= (serie) => {

		this.props.navigation.navigate('DetailSerie', {serie: serie})
	}

	render() {
		const {series} = this.state
  	//console.log(this.state.series)
  	return (
  		<View style={{flex:1, backgroundColor:'#191919' }}>
  		<FlatList
  			data={series}
  			keyExtractor={(item) => item.id.toString()}
  			renderItem={({item}) => <ItemSerie serie={item} detailSerie={this._detailSerie} />}
  		/>
  		</View>
  		)
  	}
  }

  const styles = StyleSheet.create({})

  export default ListeSerie
