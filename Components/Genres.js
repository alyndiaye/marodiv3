
import React from 'react'
import { StyleSheet, Text, View, FlatList, Dimensions } from 'react-native'
import ListCategorie from './ListCategorie'
const ITEM_WIDTH = Dimensions.get('window').width


class Genres extends React.Component {

	constructor(props) {
		super(props);

		this.state = {
			categories: [],
			columns: 2,
			isLoading: false
		};
	}

	componentDidMount() {
		this.setState({ isLoading: true })
		fetch('https://www.marodi.tv/api/categories')
		.then(response => response.json())
		.then(data => this.setState({
			categories: data.categories,
			isLoading: false
		}));
	}

	_displayGenre = (slug, genre) => {
		if (slug==="serie") {
			this.props.navigation.navigate('ListeSerie', {slug: slug, genre: genre})		

		} else {
			
			this.props.navigation.navigate('ListeVideo', {slug: slug, genre: genre})		
		}
	}

	render() {
		const {columns, categories} = this.state
		//console.log(this.props.navigation)
		return (
			<View style={{flex:1, backgroundColor: '#191919' }}>
			<FlatList
			numColumns={columns} 
			data={categories}
			keyExtractor={(item) => item.id.toString()}
			renderItem={({item}) => { return <ListCategorie itemWidth={(ITEM_WIDTH-20)/columns} categorie={item} displayGenre={this._displayGenre}/>}}
			
			/>
			</View>
			)
		}
	}

	const styles = StyleSheet.create({})

	export default Genres
