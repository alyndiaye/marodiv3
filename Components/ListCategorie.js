import React from 'react'
import {View, Image, ImageBackground, Text, StyleSheet, TouchableOpacity} from 'react-native'


class ListCategorie extends React.Component {
  constructor(props) {
    super(props);
    this.state = {

    }
  }
  render(){
    const {categorie, itemWidth,displayGenre} = this.props
  //console.log(this.props)
    return(
      <TouchableOpacity onPress={()=> displayGenre(categorie.slug, categorie.name)} style={{margin:5,}}>
        <ImageBackground
          style={{flex:1, flexDirection: 'row', height:98, width: itemWidth, backgroundColor: '#222222', borderRadius: 3,
    justifyContent: 'center',
    alignItems: 'center' }}
          source={{uri: 'marodi.tv/images/sub-header/coversketch.jpg'}}
        >
          <Text style={styles.text_name}> {categorie.name} </Text>
        </ImageBackground>
      </TouchableOpacity>
    )
  }
}
const styles = StyleSheet.create({
  container:{

  },
  bg:{

  },
  text_name:{
fontSize: 22,
color: '#E7E7E7',
// fontFamily: 'Circular Std', 
  },
  bgImage:{
    width: null,
    height: 100
  },
  textOver:{
    fontWeight: 'bold',
    color: 'white',
    position: 'absolute', // child
    bottom: 100, // position where you want
    left: 0,
    textAlign: 'center'
  }

})

export default ListCategorie
