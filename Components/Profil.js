
import React from 'react'
import { StyleSheet, Text, View, Button, TouchableOpacity, Image, ScrollView, Platform, Dimensions } from 'react-native'
import {connect} from 'react-redux'
const ITEM_WIDTH = Dimensions.get('window').width

class Profil extends React.Component {

	constructor(props) {
	  super(props);
	
	  this.state = {};
	}

	_login(){
		this.props.navigation.navigate('Login')
	}

	_register(){
		this.props.navigation.navigate('Register')
	}

	_favories(){
		this.props.navigation.navigate('Favories')
	}
	_deconnectUser(){
		const action = {type: "DISCONNECT_USER", value: this.props.user}
       this.props.dispatch(action)
       this.props.navigation.navigate('Profil')
	}

	render() {
		//console.log(this.props)
		return (
			<ScrollView style={styles.main_container}>
			{
				this.props.user === undefined ? 
				<View style={styles.auth_container}>
					<TouchableOpacity style={styles.loginStyle} onPress={() => this._login()}>	
						<Text style={styles.loginStyles}>
							SE CONNECTER
						</Text>
					</TouchableOpacity>
					<View style={styles.buttonStyle}>
						<Button
							color="#21CA7A"
							styleDisabled={{color: 'red'}}
							onPress={() => this._register()}
							title="S'INSCRIRE"
							/>
					</View>
				</View>
				:
				<View style={styles.auth_container1}>
					<Image source={require('../assets/logo.png')} style={styles.imageProfilStyle}/>
					<Text style={{color: 'white'}} >
					{this.props.user.name}
					</Text>
					<Text style={{color: '#7A7A7A'}} >
					{this.props.user.email}
					</Text>


						
				</View>
			}
					
				<TouchableOpacity onPress={() => this._favories()}>
				<View style={styles.favorie_container}>
					<Text style={styles.labelStyle}>
						Mes vidéos favories
					</Text>
					<Image source={require('../assets/right-thin-chevron.png')} style={styles.imageStyle}/>
				</View>
				</TouchableOpacity>
				<View style={styles.a_propos_container}>
					<Text style={styles.labelStyle}>
						A propos
					</Text>
					<Image source={require('../assets/right-thin-chevron.png')} style={styles.imageStyle}/>

				</View>
				<View style={styles.other_container}>
					<Text style={styles.labelStyle}>
						Version
					</Text>
					<Image source={require('../assets/right-thin-chevron.png')} style={styles.imageStyle}/>

				</View>
				<View style={styles.other_container}>
					<Text style={styles.labelStyle}>
						Confidentialités & conditions
					</Text>
					<Image source={require('../assets/right-thin-chevron.png')} style={styles.imageStyle}/>

				</View>
				<View style={styles.evaluerContainer} >
				<Text style={{color: 'white', marginTop:5}}>
				Vous aimez MarodiTV?
				</Text>
				<Text style={{color: '#7A7A7A'}}>
				Merci de noter lapplication sur {Platform.OS === 'ios' ? 'App store' : 'Play store'}
				</Text>
				<View style={styles.buttonEvalStyle}>
						<Button
							color="#21CA7A"
							onPress={() => this._register()}
							title="EVALUER"
							/>
					</View>

				</View>
				{
					this.props.user !== undefined ? 
					<TouchableOpacity onPress={()=>{this._deconnectUser()}} >

					<View style={styles.disconnectContainer} >
				<Text style={styles.labelStyle}>
						Deconnecter
					</Text>
					<Image source={require('../assets/exit.png')} style={styles.imageDStyle}/>
				</View>
					</TouchableOpacity>
				
				: <View>
				
				</View>
				}
				
		    </ScrollView>	
			)
		}
	}

	const styles = StyleSheet.create({
		main_container:{
			flex:1,
			backgroundColor: '#191919',
		},
		auth_container:{
			flexDirection: 'row',
			backgroundColor: '#1B1B1B',
			height: 98
		},
		auth_container1:{
			flexDirection: 'column',
			backgroundColor: '#1B1B1B',
			height: 120,
			alignItems: 'center',
			justifyContent: 'center'
		},
		favorie_container:{
			flexDirection: 'row',
			marginTop: 16,
			marginLeft:16,
			marginRight:16,
			borderRadius: 2,
			height: 45,
			backgroundColor: '#222222',
			alignItems: 'center',

		},
		a_propos_container:{
			flexDirection: 'row',
			marginTop: 46,
			marginLeft:16,
			marginRight:16,
			borderRadius: 2,
			height: 45,
			backgroundColor: '#222222',
			alignItems: 'center',
		},
		other_container:{
			flexDirection: 'row',
			marginTop: 8,
			marginLeft:16,
			marginRight:16,
			borderRadius: 2,
			height: 45,
			backgroundColor: '#222222',
			alignItems: 'center',
		},
		evaluerContainer:{
			height: 140,
			marginTop: 16,
			marginLeft: 30,
			marginRight: 30,
			backgroundColor: '#000000',
			alignItems: 'center'
		},
		disconnectContainer:{
			flexDirection: 'row',
			marginTop: 24,
			marginLeft:16,
			marginRight:16,
			borderRadius: 2,
			height: 45,
			backgroundColor: '#222222',
			alignItems: 'center'
		},
		labelStyle:{
			flex: 1,
			lineHeight: 18,
			marginLeft:12,
			color: '#B4B4B4',
			fontSize: 14,
			fontWeight: '300',
			letterSpacing: 0.5

		},

		loginStyles:{
			color: '#E7E7E7',
			fontSize: 14,
			letterSpacing: 0.5,
			lineHeight: 18,
			justifyContent: 'center',
			
		},loginStyle:{
			flex:1,
			flexWrap: 'wrap',
			marginTop: 40,
			marginLeft: 42
		},
		registerStyle:{
			color: '#E7E7E7',
			fontSize: 14,
			lineHeight: 18,
			justifyContent: 'center',
		},
		buttonStyle:{
			backgroundColor: '#21CA7A',
			borderRadius: 2,
			height: 36,
			width: 149,
			marginTop:31,
			marginRight:27,
		},
		buttonEvalStyle:{
			backgroundColor: '#21CA7A',
			borderRadius: 5,
			height: 36,
			marginLeft:15,
			marginRight:15,
			width: ITEM_WIDTH-76
			
		},
		imageStyle:{
			padding: 10,
			height: 2,
			width: 2
		},
		imageDStyle:{
			padding: 10,
			margin: 10,
			height: 32,
			width: 32

		},
		imageProfilStyle:{
			height: 60,
			width: 60
		}
	})

	const mapStateToProps = state => {
  return {
    user: state.user
  }
}

export default connect(mapStateToProps)(Profil)
