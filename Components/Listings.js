import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import Icon from 'react-native-vector-icons/FontAwesome';
import {
  View,
  Text,
  TouchableHighlight,
  TouchableOpacity,
  ScrollView,
  Image,
  StyleSheet,
} from 'react-native';
import HeartButton from './HeartButton';

export default class Listings extends Component {
  constructor(props) {
  	super(props);
    this.state = {
      films: []
    }
  	this.renderListings = this.renderListings.bind(this);
  }
 componentDidMount() {
    this.setState({ isLoading: true })
    fetch('https://www.marodi.tv/api/categorie/serie')
    .then(response => response.json())
    .then(data => this.setState({
      films: data.videos,
     
    }));
  }

  renderListings() {
  	const {
      films
    } = this.state;
    return films.map((film, index) => (
      <TouchableHighlight
        style={styles.card}
        key={film.id}
      >
        <View>
          
            <View style={styles.addToFavoriteBtn}>
              <HeartButton
                color={'#ffffff'}
                selectedColor={'#fc4c54'}
                //selected={favouriteListings.indexOf(listing.id) > -1}
                onPress={() => handleAddToFav(film)}
              />
            </View>
            
          <Image
            style={styles.image}
            resizeMode="contain"
            source={require('../assets/logo.png')}
          />
          <Text style={[{ color: 'red' }, styles.listingType]}>
            {film.cat}
          </Text>
          <Text
            style={styles.listingTitle}
            numberOfLines={2}
          >
            {film.description}
          </Text>
          <Text style={styles.listingPrice}>
$
            {film.id}
            {' '}
            {film.name}
          </Text>
         
        </View>
      </TouchableHighlight>
    ));
  }

  render() {
  	//const { title, boldTitle } = this.props;
  	const titleStyle = false ? { fontSize: 22, fontWeight: '600' } : { fontSize: 18 };
  	return (
    <View style={styles.wrapper}>
      <View style={styles.titleWrapper}>
        <Text style={[titleStyle, styles.title]}>
          titre
        </Text>
        <TouchableOpacity style={styles.seeAllBtn}>
          <Text style={styles.seeAllBtnText}>
See all
          </Text>
          <Icon
            name="angle-right"
            size={18}
            color={'#484848'}
          />
        </TouchableOpacity>
      </View>
      <ScrollView
        style={styles.scrollView}
        contentContainerStyle={{ paddingRight: 30 }}
        horizontal
        showsHorizontalScrollIndicator={false}
      >
        {this.renderListings()}
      </ScrollView>
    </View>
  	);
  }
}

Listings.propTypes = {
  title: PropTypes.string.isRequired,
  boldTitle: PropTypes.bool,
  listings: PropTypes.array.isRequired,
  showAddToFav: PropTypes.bool,
  handleAddToFav: PropTypes.func,
  favouriteListings: PropTypes.array,
};

const styles = StyleSheet.create({
  wrapper: {
    display: 'flex',
  },
  titleWrapper: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingLeft: 21,
    paddingRight: 21,
  },
  title: {
    color: '#484848',
  },
  seeAllBtn: {
  	marginTop: 2,
  	flexDirection: 'row',
  	alignItems: 'center',
  	justifyContent: 'space-between',
  },
  seeAllBtnText: {
  	color: '#484848',
  	marginRight: 5,
  },
  scrollView: {
    marginTop: 20,
    marginLeft: 15,
    marginBottom: 40,
  },
  card: {
    marginRight: 6,
    marginLeft: 6,
    width: 157,
    flexDirection: 'column',
    minHeight: 100,
  },
  image: {
  	width: undefined,
  	flex: 1,
  	height: 100,
  	borderRadius: 8,
  	marginBottom: 7,
  },
  listingTitle: {
    fontSize: 14,
    fontWeight: '700',
    color: '#484848',
    marginTop: 2,
  },
  listingType: {
  	fontWeight: '700',
  	fontSize: 10,
  },
  addToFavoriteBtn: {
    position: 'absolute',
    right: 12,
    top: 7,
    zIndex: 2,
  },
  listingPrice: {
  	color: '#484848',
  	marginTop: 4,
  	marginBottom: 2,
  	fontSize: 12,
  	fontWeight: '300',
  },
});